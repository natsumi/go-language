
# Switch...Case
เป็นคำสั่งใช้กำหนดเงื่อนไขคล้ายกับ if แต่จะเลือกเพียงหนึ่งทางเลือกออกมาทำงาน
โดยนำค่าในตัวแปรมากำหนดเป็นทางเลือกผ่านำสั่ง Case
***


### รูปแบบคำสั่ง

```
package main

import "fmt"

func main(){
    var number int 
    fmt.Print("INPUT") //รับค่า
    
     switch (number) {
        case 1 : 
            fmt.Println("CASE 1")
        case 2 : 
            fmt.Println("CASE 2")  
        default   : 
            fmt.Println("DEFAULT")  
    }
}
```
