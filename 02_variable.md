# Variable 
***
### ประเภทตัวแปรและหน้าที่ต่างๆ:
* string
    * ใช้สำหรับเก็บค่าตัวอักษร
* numeric
    * ใช้สำหรับเก็บค่าจำนวนเต็ม
* float
    * ใช้สำหรับเก็บค่าจำนวนจริง
* bool
    * ใช้สำหรับเก็บค่าตรรกศาสตร์
  

### การกำหนดตัวแปร
* name int
* vars string name
* var name integer
* var width int

### ภาษา Go เป็นภาษาประเภท
* Weakly-Typed
* Dynamically-Typed
* Strongly-Typed
* Freely-Typed
