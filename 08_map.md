# Maps
ตัวแปรที่เก็บข้อมูลในรูปแบบคู่ key,value มีลักษณะคล้ายกับ array แต่จะใช้ key เป็น index เพื่อเชื่อมโยงข้อมูลที่(value) ที่เก็บใน key นั้นๆ
ถ้าทราบ key ก็สามารถข้าถึง value หรือข้อมูลได้นั้นเอง
***

### การนิยาม Maps
```text
     var ชื่อตัวแปร map[key_type]value_type
     
     var country map[string]string
```
### Exp Maps
```go

func main(){
     country := map[string] string{}
     country["TH"] = "Thailand"
     country["EN"] = "England"
     
     value, check:= country["TH"]
     
     if check {
        fmt.Println(value)
     } else {
        fmt.Println("ไม่พบข้อมูล")
     }
}
```
### output
```text
Thailand
```
