# Function
* ชุดคำสั่งที่นำมาเขียนมารวมเป็นกลุ่มเพื่อให้เรียกใช้งานตามวัตถุประสงค์ที่ต้องการและลดความซ้ำซ้อนองคำสั่งที่ใช้งานบ่อยๆ
***

## รูปแบบของฟังก์ชั่น
```go
    
func main(){
    showMessage()
    
    showName("nut")
    
    total(50,100)
    
    //ต้องสร้างตัวแปรมารับเพราะเพราะมีการส่ง return ค่ามา
    delivery := getDelivery() 
    fmt.Println("ค่าส่ง = ", delivery)
    
    myTotal := getTotal(50,50) 
    fmt.Println("ยอดรวม = ", myTotal)

}
    
//ไม่มีการรับค่าและส่งค่า
func showMessage(){
    fmt.Println("Hello Go Programming")
}

//มีการรับค้ามาทำงาน
func showName(name string){
    fmt.Println("Hello ", name)
}
//สามารถเขียนยุบรวมได้ หากพารามิเตอร์มีชนิดข้อมมูลเดียวกัน
func total(num1,num2 int){ 
    fmt.Println("ยอดรวม = ", num1 + num2)
}

//มีการส่งค่าออกมา
func getDelivery() int{ 
    return 50
}

//มีการรับค่าและส่งค่า
func getTotal(num1,num2 int) int{ 
    total := num1+num2
    return total
}
```
### output
```text
Hello Go Programming
Hello nut
ยอดรวม = 150
ค่าส่ง = 50
ยอดรวม = 100

```

