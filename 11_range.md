# Range
***
 
### Exp 1 ประยุกต์ใช้งานร่วมกับ array หรือ slices
```go
func main(){

    number := []int{10,20,30,40,50,60,70,80,90,100}
    
    for index := 0; index <= len(number);  index++ {
      
        fmt.Println("Index = ",index, "Value = ", number[index])
    }

    
}
```
เป็นการประยุกต์ใช้ for loop ให้ทำงานร่วมกับ array หรือ slices

### output
```text
Index = 0 Value = 10
Index = 1 Value = 20
Index = 2 Value = 30
Index = 3 Value = 40
Index = 4 Value = 50
Index = 5 Value = 60
Index = 6 Value = 70
Index = 7 Value = 80
Index = 8 Value = 90
Index = 9 Value = 100
```

### Exp 2 Range
จาก exp 1 จะเห็นว่ามันมีการสร้าง ตัวนับจำนวนรอบขึ้นมา ตรวจสอบเงื่อนไข และมีดารเพิ่ม index ซึ่งมันดูยุ่งยากซับซ้อน
เราสามารถใช้ for loop ในอีกรูปแบบนึงได้ ดังนี้  (มันก็คือ for each ในภาษาอื่น)
```go
func main(){

    number := []int{10,20,30,40,50,60,70,80,90,100}
    
    for index, value := range number {
      
        fmt.Println("Index = ",index, "Value = ",value )
    }
}
```
### output
```text
Index = 0 Value = 10
Index = 1 Value = 20
Index = 2 Value = 30
Index = 3 Value = 40
Index = 4 Value = 50
Index = 5 Value = 60
Index = 6 Value = 70
Index = 7 Value = 80
Index = 8 Value = 90
Index = 9 Value = 100
```

เนื่องจาก range number จะได้ ค่ากลับมา 2 ตัว เป็น index และ value หากเราต้องการใช้แค่ค่าบางเราสามารถ ใส่ _ เพื่อให้มันอิกนอร์หรือว่ามองข้ามไป
```go
func main(){

    number := []int{10,20,30,40,50,60,70,80,90,100}
    
    for _, value := range number {
      
        fmt.Println( "Value = ",value )
    }
}
```
### output
```text
Value = 10
Value = 20
Value = 30
Value = 40
Value = 50
Value = 60
Value = 70
Value = 90
Value = 100
```

### Exp 3 ประยุกต์ใช้งานร่วมกับ Maps
#### Exp 3.1
```go
func main(){

    language := map[string]string{"TH" : "Thai", "EN" : "English"}
    
    for key, value := range language {
      
        fmt.Println("Key = ",key, "Value = ",value )
    }

    
}
```
### output
```text
Key = TH Value = Thai
Key = EN Value = English
```

#### Exp 3.2
```go
func main(){

    language := map[string]string{"TH" : "Thai", "EN" : "English"}
    
    for  _, value := range language {
      
        fmt.Println( "Value = ",value )
    }

    
}
```
### output
```text
Value = Thai
Value = English
```