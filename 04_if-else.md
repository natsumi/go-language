
# If...Else
เป็นคำสั่ง่ใช้กำหนดเงื่อนไขการตัดสินใจทำงานของโปรแกรม 
ถ้าเงื่อนไขเป็นจริงจำทำตามคำสั่งต่างๆ ภายใต้เงื่อนไขนั้นๆ

***

### if statement (คำสั่งแบบเงื่อนไขเดียว)

```
package main

import "fmt"

func main(){
    var score int 
    fmt.Print("INPUT") //รับค่า
    
    if score >= 50 {
        fmt.Println("OUTPUT")
    }
}
```

### if…else (คำสั่งแบบ 2 เงื่อนไข)
```
package main

import "fmt"

func main(){
    var number int 
    fmt.Print("INPUT") //รับค่า
    
    if number >= 50 {
        fmt.Println("OUTPUT")
    } else {
        fmt.Println("OUTPUT")
    }
}
```

### if กับ else if (คำสั่งหลายเงื่อนไข)
```
package main

import "fmt"

func main(){
    var age int 
    fmt.Print("INPUT") //รับค่า
    
    if age >= 20 {
        fmt.Println("วัยรุ่น")
    } else if age >= 30 {
        fmt.Println("วัยทำงาน")
    } else if age >= 40 {
        fmt.Println("วัยกลางคน")
    } else if age >= 50 {
        fmt.Println("วัยชรา")
    } else {
        fmt.Println("จบการทำงาน")
    } 
}
```

### if กับ else if (คำสั่งหลายเงื่อนไข)
```
package main

import "fmt"

func main(){
    var age int 
    fmt.Print("INPUT") //รับค่า
    
    if age >= 20 {
        fmt.Println("วัยรุ่น")
    } else if age >= 30 {
        fmt.Println("วัยทำงาน")
    } else if age >= 40 {
        fmt.Println("วัยกลางคน")
    } else if age >= 50 {
        fmt.Println("วัยชรา")
    } else {
        fmt.Println("จบการทำงาน")
    } 
}
```

### If แบบ มี Process ก่อนเข้าเงื่อนไข
 Go นั้นสามารถ Process ค่าก่อนเข้าเงื่อนไขแบบนี้ได้
```go
func main(){   

        i := 2;
        j := 3;
        k := 0;
        
        if k = i + j; k == 5 {
           println("k มีค่าเท่ากับ 5");
        }
}
```
