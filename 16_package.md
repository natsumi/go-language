# Package
* สิ่งที่ช่วยให้ผู้พัฒนาโปรแกรมสามารถจัดการโค้ด เป็นกลุ่มการทำงานได้ ด้วยวิธีการแยกไฟล์ออกเป็นส่วนๆตามรูปแบบการทำงาน ซึ่งมาสารถเรียกใช้ภายในส่วนต่างๆของโปรแกรมได้ ส่งผลให้โค้ดเป็นระเบียบละนำกลับมาใช้งานใหม่ได้ง่าย
***

## ตัวอย่าง Package
```text
    
import "fmt" -> นำ Package มาทำงาน

fmt.Println("Hello Go Programming")
-> fmt เรียก Package มาใช้
-> Println เรียกใช้ฟังก์ชั่นใน Package 
```



## Exp 1 package calculator 
* ชื่อสินค้า(string)
* ราคา(float)
* หมวดหมู่(string)
* ส่วนลด(int)

#### ไฟล์ app.go (อยู่ใน folder calculator)
```go

package calculator

/* 
การนิยามฟังก์ชั่น อักษรตัวแรกระบุเป็นตัวพิ มพ์ใหญ่
ความหมาย คือ  ฟังชั่นนี้จะให้บริการกับไฟล์ ที่อยู่ข้องนอก package ด้วย
*/

func Add(num1,num2 int) int{
    return num1 + num2
}
func Suptract(num1,num2 int) int{
    return num1 - num2
}

```
#### ไฟล์ main.go
```go

package main

import (
    "fmt"
    "go-language/calculator" //ชื่อ project/ชื่อ calculator
)


func main(){
    fmt.Println(calculator.Add(10,5))
    fmt.Println(calculator.Suptract(20,10))
}

```
#### output
```text
15
10
```
