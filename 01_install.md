# GO INSTALLATION GUIDES

## NOTE

หากคุณติดตั้ง [homebrew](https://brew.sh) คุณสามารถติดตั้ง Go ได้อย่างง่ายดาย:

```
# คำสั่งในการ install go
brew install go

```

## Install Go

1. ไปที่ [https://golang.org/dl](https://golang.org/dl)
2. เลือก OS X (Mac)
3. ดาวน์โหลด เปิดไฟล์ทำการติดตั้ง



