# Return Multiple Value
* 
***

## รูปแบบของฟังก์ชั่น
```go
    
func main(){
     result,check:= summation(100,200)
     fmt.Println("ยอดรวม = ", result )
     fmt.Println(check)
}
    

func summation(num1,num2 int) (int,string){ 
    total := num1+num2
    status := ""
    if total%2 == 0{
        status = "เลขคู่"
    }else{
        status = "เลขคี่"
    }
    return total,status
}

```
### output
```text
ยอดรวม = 300
เลขคู่
```

