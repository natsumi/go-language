# Slices
คลายกับ array แต่มีความสามารถในการปรับเปลี่ยนขนาดสมาชิกได้ (dinamic size)
***
### การนิยาม slices

```go
func main(){
     number := []int{1,2}
     
     fmt.Println(number)
}
```
### output
```text
[1 2]
```

### การเพิ่ม slices

```go
func main(){
     number := []int{1,2}
     
     number = append(number,3)
     number = append(number,4)
     
     fmt.Println(number)
}
```
### output
```text
[1 2 3 4]
```
### การเข้าถึง slices

```go
func main(){
     number := []int{1,2}
     
     number = append(number,3)
     number = append(number,4)
     
     fmt.Println(number[0])
     fmt.Println(number[1])
}
```
### output
```text
[1]
[2]
```
### การเข้าถึงสมาชิกแบบกำหนดช่วง

```go
func main(){
     number := []int{1,2}
     
     number = append(number,3)
     number = append(number,4)
     
     fmt.Println(number[:])
     fmt.Println(number[1:]) // index 1 - สุดท้าย
     fmt.Println(number[1:3]) // index 1 - 2 (ค่าสุดท้ายต้องใส่เป็นตำแหน่งสุดท้ายที่อยากได้ +1)
     fmt.Println(number[:3])
}
```
### output
```text
[1 2 3 4]
[2 3 4]
[2 3]
[1 2 3]
```