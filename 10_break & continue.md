# Break & Continue (คำสั่งที่เกี่ยวข้องกับ loop)
* Break
  * ถ้าโปรแกรมพบคำสั่งนี้จะหลุดหลุดการทำงานจากลูปทันทีเพื่อไปทำคำสั่งอื่นที่อยู่นอกลูป
* Continue
  * จะหยุดการทำงานแล้วย้อนกลับไปเริ่มต้นการทำงานที่ต้นลูปใหม่
***

### Exp Break
```go
func main(){
    for count:= 1; count <= 5;  count++ {
        if count == 3 {
            continue
        }
        fmt.Println(count)
    }
    fmt.Println("จบโปรแกรม")
    
}
```
### output
```text
1
2
จบโปรแกรม
```

### Exp Continue
```go
func main(){
    for count:= 1; count <= 5;  count++ {
        if count == 3 {
            continue
        }
        fmt.Println(count)
    }
    fmt.Println("จบโปรแกรม")
    
}
```
### output
```text
1
2
4
5
จบโปรแกรม
```
