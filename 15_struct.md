# Struct
* ข้อมูลแบบโครงสร้าง ที่นำเอาข้อมูลที่มีชนิดข้อมูลต่างกันมารวบรวมเข้าด้วยกัน แต่มีความสัมพันธ์ของข้อมูลแบบต่อกัน มาเก็บไว้ภายในโครงสร้างเดียวกัน
***

## การนิยาม Struct
```text
    
type ชื่อ struct{
    ตัวแปรที่ 1 ชนิดข้อมูลตัวที่ 1
    ตัวแปรที่ 2 ชนิดข้อมูลตัวที่ 2
}

```



## Exp 1 เก็บข้อมูลสินค้า(Product)
* ชื่อสินค้า(string)
* ราคา(float)
* หมวดหมู่(string)
* ส่วนลด(int)
```go

package main

import "fmt"

type Product struct{
    name string
    price float64
    category string
    discount int
}

func main(){
    product1 := Product{name: "ปากกา", price: 50.5, category: เครื่องเขียน, discount: 10}
    fmt.Println(product1)
    fmt.Println(product1.name ) //การเข้าถึงข้อมูล
}

```
### output
```text
{ปากกา 50.5 เครื่องเขียน 10}
ปากกา
```

